#include <stdio.h>
#include <stdlib.h>

//Para uma arvore de ordem M
#define MAX_FILHOS 4 //Quantidade máxima de filhos (M)
#define MAX_CHAVES 3 //Quantidade máxima de chaves (M-1)
#define MIN_OCUP 1 //Ocupação mínima em cada nó (M/2 - 1)


typedef struct no_arvoreB arvoreB;

struct no_arvoreB {
    int num_chaves; //Quantidades de chaves contida no nó
    int chaves[MAX_CHAVES]; //Chaves armazenadas no nó
    arvoreB *filhos[MAX_FILHOS]; //Ponteiro para os filhos
};

int busca_binaria(arvoreB *no, int valor) //função de busca binária em uma página (vetor)
{
  int meio, inicio, fim;
  inicio = 0;
  fim = no->num_chaves-1;

  while (inicio  <= fim)
  {
    meio = (inicio  + fim)/2;
    if (no->chaves[meio] == valor)
       return meio; // Se encontrou o valor buscado, retorna sua posição
    else if (no->chaves[meio] > valor)
            fim = meio - 1;
         else inicio = meio + 1;
  }
  return inicio; //Se não encontrou, retorna a posição do ponteiro para o filho.
}

int busca(arvoreB *raiz, int valor)
{
  arvoreB *no;
  int pos; //posição retornada pelo busca binária.

  no = raiz;
  while (no != NULL)
   {
     pos = busca_binaria(no, valor);
     if (pos < no->num_chaves && no->chaves[pos] == valor)
         return 1;
     else no = no->filhos[pos];
   }
  return 0;
}


void imprime_in_ordem(arvoreB *raiz)
{
  int i;
  if (raiz != NULL)
   {
     for (i = 0; i < raiz->num_chaves; i++)
      {
        imprime_in_ordem(raiz->filhos[i]);
        printf("\n%d", raiz->chaves[i]);
      }
      imprime_in_ordem(raiz->filhos[i]);
    }
}



int main()
{

//criando uma na mãozona
arvoreB *a1 = (arvoreB *) malloc (sizeof(arvoreB));
a1->num_chaves = 1;
a1->chaves[0] = 20;

arvoreB *a2  = (arvoreB *) malloc (sizeof(arvoreB));
a2->num_chaves=2;
a2->chaves[0]=5;
a2->chaves[1]=9;

arvoreB *a3  = (arvoreB *) malloc (sizeof(arvoreB));
a3->num_chaves=2;
a3->chaves[0]=25;
a3->chaves[1]=30;

arvoreB *a4  = (arvoreB *) malloc (sizeof(arvoreB));
a4->num_chaves=2;
a4->chaves[0]=1;
a4->chaves[1]=2;

arvoreB *a5  = (arvoreB *) malloc (sizeof(arvoreB));
a5->num_chaves=2;
a5->chaves[0]=6;
a5->chaves[1]=8;

arvoreB *a6  = (arvoreB *) malloc (sizeof(arvoreB));
a6->num_chaves=2;
a6->chaves[0]=13;
a6->chaves[1]=16;

arvoreB *a7  = (arvoreB *) malloc (sizeof(arvoreB));
a7->num_chaves=3;
a7->chaves[0]=22;
a7->chaves[1]=23;
a7->chaves[2]=24;


arvoreB *a8  = (arvoreB *) malloc (sizeof(arvoreB));
a8->num_chaves=2;
a8->chaves[0]=28;
a8->chaves[1]=29;

arvoreB *a9  = (arvoreB *) malloc (sizeof(arvoreB));
a9->num_chaves=3;
a9->chaves[0]=31;
a9->chaves[1]=32;
a9->chaves[2]=323;

a1->filhos[0]=a2;
a1->filhos[1]=a3;
a1->filhos[2]=NULL;
a1->filhos[3]=NULL;

a2->filhos[0]=a4;
a2->filhos[1]=a5;
a2->filhos[2]=a6;
a2->filhos[3]=NULL;

a3->filhos[0]=a7;
a3->filhos[1]=a8;
a3->filhos[2]=a9;
a3->filhos[3]=NULL;


a4->filhos[0]=NULL;
a4->filhos[1]=NULL;
a4->filhos[2]=NULL;
a4->filhos[3]=NULL;

a5->filhos[0]=NULL;
a5->filhos[1]=NULL;
a5->filhos[2]=NULL;
a5->filhos[3]=NULL;

a6->filhos[0]=NULL;
a6->filhos[1]=NULL;
a6->filhos[2]=NULL;
a6->filhos[3]=NULL;

a7->filhos[0]=NULL;
a7->filhos[1]=NULL;
a7->filhos[2]=NULL;
a7->filhos[3]=NULL;

a8->filhos[0]=NULL;
a8->filhos[1]=NULL;
a8->filhos[2]=NULL;
a8->filhos[3]=NULL;

a9->filhos[0]=NULL;
a9->filhos[1]=NULL;
a9->filhos[2]=NULL;
a9->filhos[3]=NULL;


imprime_in_ordem(a1);
/*    arvoreB *arv = NULL;

    arv = insere_arvoreB(arv, 10);
    imprime_in_ordem(arv);
    getch();
    arv = insere_arvoreB(arv, 300);
    imprime_in_ordem(arv);
    getch();
    arv = insere_arvoreB(arv, 100);
    imprime_in_ordem(arv);
    getch();
    arv = insere_arvoreB(arv, 30);
    imprime_in_ordem(arv);
    getch();
    arv = insere_arvoreB(arv, 11);
    imprime_in_ordem(arv);
    getch();
    arv = insere_arvoreB(arv, 77);
    imprime_in_ordem(arv);
    getch(); */

    return 1;
}
